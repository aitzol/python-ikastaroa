# Python Ikastaroa #

Repositorio honetan Pythoni buruzko ikastaro baterako materiala dago ipython notebook formatuan

## Nola konfiguratu? ##

* Virtualenv bat sortu
* clonatu repositorioa
* dependentziak instalatu

## Notebooka editatu ##
## Aurkezpen formatuan ikusi ##
## Lagundu osatzen ##

### Virtualenv bat sortu ###

Komenigarria da virtualenv bat sortzea. Horretarako exekutatu

```shell
virtualenv _venv-izena_
```

** OHARRA: ** Kode hau python3rako prestatuta dago, hortaz, python3 ez bada zure sistemako defektuzko interpretea beste komando hau erabili beharko duzu

```shell
virtualenv -p /path/to/python3 _venv-izena_
```

## clonatu repositorioa ##

Egin repositorio honen fork bat edo zuzenean klonatu

## Dependentziak instaltu ##

Virtualenv-a aktibo dagoela exekutatu

```shell
pip install -r requirements.txt
```

### Notebooka editatu ###

Virtualenv-aren barruan exekutatu

```shell
jupyter notebook
```

Nabegadorean agertuko zaizkizu aukeran dauden gai zerrenda. Bertan editatu edo kodea frogatu dezakezu

### Aurkezpen formatuan ikusi ###

Notebook-ak aurkezpen bezala ikusteko exekutatu

```shell
ipython nbconvert NOTEBOOK-FITXATEGIA --to slides --post serve
```

### Lagundu osatzen ###

Okerren bat aurkitzen baduzu, ariketaren bat gehitu nahi baldin baduzu, falta den gairen bat ikusten baldin baduzu, gustura hartuko ditut pull requestak