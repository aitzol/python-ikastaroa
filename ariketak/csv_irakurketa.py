import json

def loadDataAtOnce(fitx):
    data = []
    for cam in fitx.readlines():
        data.append(cam.split(';'))
    return data

def createHiztegia(data_list, gakoak):
    return {gakoak[0]: data_list[0],
            gakoak[1]: data_list[1],
            gakoak[2]: data_list[2]}

def createHiztegia2(data_list, gakoak):
    h = {}
    for k,v in zip(gakoak, data_list):
        h[k] = v
    return h

if __name__ == '__main__':
    emaitza = []
    gakoak = ''
    with open('Camera.csv') as f:
        gakoak = f.readline()
        gakoak = gakoak.split(';')
        datuak = loadDataAtOnce(f)
        for datua in datuak:
            emaitza_d = createHiztegia2(datua, gakoak) 
            emaitza.append(emaitza_d)
    with open('camera.json', 'w') as w:
        json.dump(emaitza, w)

