def bikoitiak(zenbakiak):
    bikoitien_zerrenda =  []
    for zenb in zenbakiak:
        if zenb % 2 == 0:
            bikoitien_zerrenda.append(zenb)
    return bikoitien_zerrenda
