
def getZenbakia():
    emaitza = None
    while emaitza is None:
        try:
            emaitza  = int(input('Sartu zenbaki bat?'))
        except ValueError:
            print('Badirudi ez dela zenbaki bat. Saiatu berriro mesedez')
        except:
            print('Errore ezezaguna')
            break;
    return emaitza

def batu(bat, bi):
    return bat + bi

if __name__ == '__main__':
    zenbakia = getZenbakia()
    emaitza = 0
    while zenbakia != 0:
        emaitza = batu(emaitza, zenbakia)
        zenbakia = getZenbakia()
    print(emaitza)
    
        
        
        
