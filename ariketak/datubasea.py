# https://docs.python.org/3.5/library/sqlite3.html
import sqlite3

# https://docs.python.org/3.4/library/csv.html#csv.reader
import csv

CREATION_SQL = """
CREATE TABLE kamerak
(model text, release_date text, max_resolution text, low_resolution text, effective_pixels text, 
zoom_wide text, zoom_tele text, normal_focus_range text, macro_focus text, storage_included text, weight text,
dimmensions text, price text);
"""

INSERTION_SQL = """
INSERT INTO kamerak VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)
"""

TRUNCATE_TABLE_SQL = """
TRUNCATE TABLE kamerak
"""

DROP_TABLE_SQL = """
DROP TABLE kamerak
"""

def createDatabase(dbname):
    pass

def createTable(dbconn, drop_if_exists=False):
    pass

def insertCamera(dbconn, cameradata):
    pass

if __name__ == '__main__':
    # Sortu datubasea
    # Sortu taula
    # Zabaldu kamaren datu fitxategia
    #     irakurri datuak (HINT csv liburutegia erabili dezakezue)
    #     idatzi datubasean kamararen informazioa


