#!/usr/bin/python

from operazioak import gehi, ken, bider, zati
print ("Ongi etorri kalkulagailura. Aukeratu zure eragiketa:")
print ("(A) batuketa | (B) kenketa | (C) biderketa | (D) zatiketa:")
op = input("")
if op == 'A':
    num1 = int(input("Batuketaren lehen zenbakia: "))
    num2 = int(input("Batuketaren bigarren zenbakia: "))
    resul = gehi(num1, num2)
elif op == 'B':
    num1 = int(input("Kenketaren lehen zenbakia: "))
    num2 = int(input("Kenketaren bigarren zenbakia: "))
    resul = ken(num1, num2)
elif op == 'C':
    num1 = int(input("Biderketaren lehen zenbakia: "))
    num2 = int(input("Biderketaren bigarren zenbakia: "))
    resul = bider(num1, num2)
elif op == 'D':
    num1 = int(input("Zatiketaren lehen zenbakia: "))
    num2 = int(input("Zatiketaren bigarren zenbakia: "))
    resul = zati(num1, num2)
else:
    print ("Mesedez A, B, C edo D aukeratu.")

print (resul)
